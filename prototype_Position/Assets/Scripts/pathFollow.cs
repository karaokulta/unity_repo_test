﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pathFollow : MonoBehaviour
{

  [SerializeField]
  private Transform[] path; //arreglo de nodos

  [SerializeField]
  private int currentPoint = 0; //apuntador al nodo actual

  [SerializeField]
  private Transform[] players; //arreglo de jugadores/karts

  private int checkPoints = 0; //contador de nodos totales recorridos
  private int lap = 0; //contador de ciclos completos al arreglo de nodos

  private float initMaxSpeed; //valor de velocidad inicial
  private float minSpeed = 10.0f; //valor de velocidad minima
  private float reachDistance = 1.0f; //radio de distancia de los nodos
  private float collisionReach = 5.0f; //radio de distancia para la colision

  public float maxSpeed = 5.0f; //velocidad maxima
  public float trackArea = 11.0f; //radio del area de la pista
  public float maxForce = 5f; //factor de relacion
  public float mass = .2f; //masa del objeto
  public float frictionRatio = .5f; //valor de ratio de friccion



  Vector3 collisionDistance; //vector de distancia para la colision
  Vector3 velocity; //vector de la velocidad actual
  Vector3 desiredVelocity; //vector de la velocidad deseada
  Vector3 playerDistance; //vector de direccion
  Vector3 steering; //vector de giro
  Vector3 pathDistance; //vector de direccion en el arreglo
  Vector3 newPos; //vector de nueva posicion
  Vector3 bump; //vector de rebote
  Vector3 distance; //vector de distancia para la aceleracion
  Vector3 steeringForce;
  Vector3 distToObjective;
  Vector3 distToNextObjective;
  Vector3 DistToPath;

  position myPosition; //objeto de position.cs

  // Use this for initialization
  void Start()
  {
    //obten los componentes de position.cs
    myPosition = gameObject.GetComponent<position>();
    initMaxSpeed = maxSpeed;
  }

  Vector3 Seek(Vector3 pos, Vector3 obj, float mag)
  {
    Vector3 forceDir = obj - pos;
    forceDir.Normalize();
    return forceDir * mag;
  }

  // Update is called once per frame
  void Update()
  {
    steeringForce = Seek(transform.position, path[currentPoint].position, maxForce);
    distToObjective = path[currentPoint].position - myPosition.getPathPos();
    DistToPath = myPosition.getPathPos() - transform.position;

    for (int i = 0; i < players.Length; ++i)
    {
      if (this.gameObject != players[i].gameObject)
      {
        collisionDistance = players[i].transform.position - this.transform.position;
        if (collisionDistance.magnitude < collisionReach)
        {
          steeringForce += -collisionDistance * frictionRatio;
        }
      }
    }

    if (DistToPath.magnitude > trackArea)
    {
      if(maxSpeed > minSpeed)
      maxSpeed -= .3f * frictionRatio;
      //DistToPath.Normalize();
      //DistToPath *= DistToPath.magnitude * maxForce;
      steeringForce += DistToPath * frictionRatio;
    }
    else
    {
      if(maxSpeed < initMaxSpeed)
        maxSpeed += .1f;
    }

    if(distToObjective.magnitude < trackArea)
    {
      if (currentPoint != path.Length - 1)
      {
        distToNextObjective = 
          Seek(this.transform.position, path[currentPoint + 1].position, maxForce);
      }
      else
      {
        distToNextObjective =
          Seek(this.transform.position, path[0].position, maxForce);
      }

      steeringForce += distToNextObjective;

      if (maxSpeed > minSpeed)
        maxSpeed -= .3f * frictionRatio;
    }
    else
    {
      if (maxSpeed < initMaxSpeed)
        maxSpeed += .1f;
    }
    newPos =
      velocity +
      bump +
      (steeringForce * ((1.0f / mass) * Time.deltaTime))
      ;

    newPos.Normalize();
    velocity = newPos;
    newPos *= maxSpeed;

    transform.position += newPos * Time.deltaTime;
    transform.LookAt(transform.position + velocity);
    bump = Vector3.zero;

    if (distToObjective.magnitude < reachDistance)
    {
      ++currentPoint;
      ++checkPoints;
    }

    if (currentPoint >= path.Length)
    {
      currentPoint = 0;
      ++lap;
    }
  }

  public Transform[] getPath()
  {
    return path;
  }

  public int getCurrentPoint()
  {
    return currentPoint;
  }

  public int getCheckPoints()
  {
    return checkPoints;
  }

  public int getLap()
  {
    return lap;
  }
  public float getFrictionRatio()
  {
    return frictionRatio;
  }

  public Vector3 getNewPos()
  {
    return newPos;
  }


  private void OnDrawGizmos()
  {
    if (path.Length > 0)
      for (int i = 0; i < path.Length; ++i)
      {
        if (path[i] != null)
        {
          Gizmos.DrawSphere(path[i].position, reachDistance);
          if (i != 0)
            Gizmos.DrawLine(path[i - 1].position, path[i].position);
          else
            Gizmos.DrawLine(path[path.Length - 1].position, path[i].position);
        }
      }
  }

  void OnTriggerEnter(Collider other)
  {
    distance = other.transform.position - this.transform.position;
    distance.Normalize();
    bump = -distance * frictionRatio;
  }
}

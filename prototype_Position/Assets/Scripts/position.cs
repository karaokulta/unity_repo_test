﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class position : MonoBehaviour {
  private Transform[] path; //arreglo de nodos

  private int currentPoint; //apuntador al nodo actual
  private int lap; //vuelta

  private float shadow; //sombra

  [SerializeField]
  private GameObject playerPos; //objeto de GameObject para la posicion sobre el arreglo

  pathFollow myPath; //objeto de pathFollow.cs

  Vector3 pathVec; //
  Vector3 playerVec; //
  Vector3 pathPos; //
  Vector3 distance;

  // Use this for initialization
  void Start () {

    //obten los componentes de pathFollow.cs
    myPath = gameObject.GetComponent<pathFollow>();

    //obten el arreglo de nodos
    path = myPath.getPath();

	}
	
	// Update is called once per frame
	void Update () {

    //obten el nodo al que se apunta actualmente
    currentPoint = myPath.getCurrentPoint();

    //obten el # de vueltas
    lap = myPath.getLap();

    if (currentPoint == 0)
    {
      //
      pathVec = path[currentPoint].position - path[path.Length - 1].position;
      //
      playerVec = transform.position - path[path.Length - 1].position;
    }
    else
    {
      pathVec = path[currentPoint].position - path[currentPoint - 1].position;
      playerVec = transform.position - path[currentPoint - 1].position;
    }
    pathVec.Normalize();

    shadow = Vector3.Dot(playerVec, pathVec);

    if (currentPoint == 0)
    {
      if (shadow < 0)
        pathPos = path[path.Length - 1].position;
      else
      {
        pathPos = (pathVec * shadow) + path[path.Length - 1].position;
      }
    }
    else
    {
      if (shadow < 0)
        pathPos = path[currentPoint - 1].position;
      else
      {
        pathPos = (pathVec * shadow) + path[currentPoint - 1].position;
      }
    }
    playerPos.transform.position = pathPos;
  }
  public int getLap()
  {
    return lap;
  }

  public int getCheckPoints()
  {
    return myPath.getCheckPoints();
  }

  public float getShadow()
  {
    return shadow;
  }

  public Vector3 getPathPos()
  {
    return pathPos;
  }

  public Vector3 getPlayerVec()
  {
    return playerVec;
  }
}

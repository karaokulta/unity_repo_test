﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class score : MonoBehaviour
{
  [SerializeField]
  private GameObject[] players;

  position[] myPosition;

  struct playerScore
  {
    public int points;
    public float shadow;
    public int index;
    public string name;
  }

  playerScore[] PS;

  List<playerScore> scoreBoard = new List<playerScore>();

  // Use this for initialization
  void Start()
  {
    myPosition = new position[players.Length];
    PS = new playerScore[players.Length];
    for (int i = 0; i < players.Length; ++i)
    {
      myPosition[i] = players[i].GetComponent<position>();
      PS[i].index = i;
      PS[i].name = myPosition[i].name;
      scoreBoard.Add(PS[i]);
    }
  }

  // Update is called once per frame
  void Update()
  {
    for (int i = 0; i < players.Length; ++i)
    {
      PS[i].points = myPosition[i].getLap() +
  myPosition[i].getCheckPoints();
      PS[i].shadow = myPosition[i].getShadow();
      scoreBoard[i] = PS[i];
    }
    scoreBoard.Sort(delegate (playerScore x, playerScore y)
    {
      int points = y.points.CompareTo(x.points);

      if (points == 0)
      {
        points = y.shadow.CompareTo(x.shadow);
      }

      return points;
    });

    //for (int i = 0; i < players.Length; ++i)
    //{
    //  Debug.Log(scoreBoard[i].name);
    //  Debug.Log(scoreBoard[i].points);
    //  Debug.Log(scoreBoard[i].shadow);
    //}


  }
}